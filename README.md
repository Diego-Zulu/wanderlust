# Wanderlust

Obligatorio realizado por Diego Zuluaga (173642) para la materia Desarrollo de interfaces de usuario. [Wanderlust](https://wanderlust-backend.firebaseapp.com/)

## Descripción

La idea era basarnos en páginas de recomendaciones de destinos turisticos como [Airbnb Guidebook](https://www.airbnb.com/things-to-do) para realizar una página de estas páginas pero de nuestra propia autoria, utilizando react-redux. A partir de la página creada, y usando react-native, la idea era producir una app con funcionalidades similares a la página web. Por disponibilidad de dispositivos de testeo, hice la app centrada en Android.

## Tecnologías usadas

* [React](https://facebook.github.io/react/)
* [Redux](http://redux.js.org/) 
* [Firebase](https://firebase.google.com)  - Base de datos, hosting y file storage
* [react-bootstrap](https://react-bootstrap.github.io/) 
* [react-native](https://facebook.github.io/react-native/)
* [wanderlust-vr](https://bitbucket.org/Diego-Zulu/wanderlust-vr) - Sitio web que hice para la visualización de imágenes VR (se puede entrar desde Wanderlust normal, cliceando en imágenes que se pueden ver en VR)

## Instalación

Desde la carpeta wanderlust, hacemos:

```
npm install
```
* Para correr el sitio web:
```
npm run web-start
```
* Para instalar la app en un celular:
```
react-native run-android
```

## Estructuras de  la aplicación

### Estructura de componentes

```
Index
. 
└─── Root
      └─── Layout
            ├── Main
            │    ├── Footer
            │    │      └── FilterLink
            │    │
            │    └── VisiblePlaceList
            │           └── PlaceList
            │                  └── Place
            │
            ├── PlaceDetails
            │    ├── PlacePhotosShowcase
            │    │
            │    ├── ActivitiesList
            │    │      └── Activity
            │    │ 
            │    └── ReviewsManager
            │           ├── AddReview
            │           └── ReviewList
            │                 └── Review
            │
            │             
            ├── MyTrip
            │     └── PlacePhotosShowcase
            │           └── MyPlacesShowcase
            │                  └── TripPlace
            └── AuthPanel

```

### State json (resumido)
```
{
  byId: {
    '0': {
      activities: [
        {
          description: 'See the top attractions of Montevideo on a private shore excursion with your very own guide. You’ll visit several coastal barrios, stroll the Rambla of Montevideo —  a coastal esplanade — and view historic landmarks in the capital such as Independence Plaza, Constitution Square and Parliament Palace. Along the way, learn about Montevideo history, architecture and culture. Opt to extend your half-day excursion with a winemaking tour at a local vineyard and a tasting of Uruguay’s award-winning tannat wine.',
          id: 0,
          name: 'Montevideo Shore Excursion: Private Sightseeing Tour with Optional Winery Tour and Tasting',
          price: 'UYU 1100'
        },
        {
          description: 'Swap the Uruguayan capital for the coastal resort town of Punta del Este. This guided day trip from Montevideo covers main attractions including the Mediterranean-style villa and art gallery of Casa Pueblo. See the fishing town of La Barra del Maldonado, and visit Playa Mansa and Playa Brava, known for its famous sculpture of fingers emerging from the sand.',
          id: 1,
          name: 'Punta del Este Day Trip from Montevideo',
          price: 'UYU 1500'
        }
      ],
      description: 'Montevideo, a port city and the capital of Uruguay, is a perfect destination for travelers looking for a relaxing stroll past colonial era buildings and along beautiful beaches. The Ciudadela Gateway, the only remaining section of the wall that once surrounded the entire city, now serves as the entrance to Ciudad Vieja, Montevideo\'s oldest neighborhood. Home to churches, museums, and theaters, at night the area comes alive with nightclubs playing Tango and Candombe music for crowds of dancers.',
      id: 0,
      name: 'Montevideo',
      photos: [
        {
          id: 0,
          link: 'http://humanidadintegrada.org/wp-content/uploads/2014/11/Montevideo-300x300.jpg',
          vr: false
        },
        {
          id: 1,
          link: 'http://www.mollyontheroad.com/wp-content/uploads/2015/11/12298968_944402162263091_1240511631_n-300x300.jpg',
          vr: false
        },
        {
          id: 2,
          link: 'https://firebasestorage.googleapis.com/v0/b/wanderlust-backend.appspot.com/o/montevideo_vr_thumbnail.png?alt=media&token=d6fdbbb6-5f44-43ad-9f1f-0fc99a370cf5',
          vr: true,
          vr_link: 'https://wanderlust-vr.firebaseapp.com/?place=0'
        }
      ],
      region: 'South America',
      reviews: {
        lKtfEMxrRqfouVs14wf7guzi8aJ3: {
          content: 'skajdf;lksdf',
          id: 'lKtfEMxrRqfouVs14wf7guzi8aJ3',
          positiveVote: true,
          username: 'moana@gmail.com'
        },
        nKkaYCyyQlgXyB4j82o56FDWqhI3: {
          content: 'hola',
          id: 'nKkaYCyyQlgXyB4j82o56FDWqhI3',
          positiveVote: true,
          username: 'raul@gmail.com'
        }
      },
      order: 3
    },

    .
    .
    .

    ,
  places: [
    0,
    1,
    2,
    3,
    4,
    5
  ],
  userplaces: [
    2,
    5,
    0
  ],
  signIn: {
    currently: 'LOGGED_IN',
    username: 'raul@gmail.com',
    uid: 'nKkaYCyyQlgXyB4j82o56FDWqhI3'
  }
}
```
### Actions json 

```
[{
      type: 'REQUEST_PLACES'
},
{
      type: 'RECEIVE_PLACES',
      filter,
      response
},
{
      type: 'ADD_PLACE_TO_TRIP',
      userID,
      placeID
},
{
      type: 'FINISHED_ADDING_PLACE_TO_TRIP',
},
{
      type: 'FAILED_ADDING_PLACE_TO_TRIP',
      error
},
{
      type: 'DELETE_PLACE_FROM_TRIP',
      userID,
      placeID
},
{
      type: 'FINISHED_DELETING_PLACE_FROM_TRIP',
},
{
      type: 'FAILED_DELETING_PLACE_FROM_TRIP',
      error
},
{
      type: 'ADD_REVIEW_SUCCESS'
},
{
      type: 'REQUEST_PLACES_FOR_TRIP'
},
{
      type: 'RECEIVE_PLACES_FOR_TRIP',
      userID,
      response
},
{
      type: 'REORDERING_TRIP_LIST'

},
{
      type: 'LOGIN_USER',
      uid
      username

},
{
      type: 'ATTEMPTING_LOGIN'
},
{
      type: 'LOGOUT'
}]
```
## Cuentas registradas en el sitio web

```
Email: moana@gmail.com
Pass: 123456
```
```
Email: raul@gmail.com
Pass: 123456
```


## Cosas que me gustaría mejorar pero no tuve tiempo

* Puedo clickear muchas veces en los íconos de cambiar orden de los lugares en los viajes y causar que aparezcan números mas grandes de lo que deberían. Esto no es ideal pero tampoco perjudicará nuestro programa ya que la próxima vez que se seleccione bien un orden se arreglará.

* El diseño de la página fue hecho todo con [react-bootstrap](https://react-bootstrap.github.io/). A pesar de que ahora mi página tiene un look muy "default", en un futuro voy a poder buscar templates de bootstrap para usar. También me gustaría tener mi propio logo y fondo para la página.

* El diseño de la app también quedo un poco básico, pero principalmente porque quise mantener la estética del sitio web, para que un posible usuario tenga el menor tiempo de aclimatación al cambiarse del sitio web a la app, o vice versa.

* No hay paginado. Decidí dejar esta funcionalidad para lo último ya que me parecía poco importante siempre y cuando yo sepa exactamente que ítems van a aparecer por pantalla o no (ya que yo hice la base de datos). Sin embargo, capaz estaría bueno hacerle paginado más adelante.

* Las regiones son hardcodeadas con respecto al servidor. Estaría bueno que me ofreciera filtrar dinámicamente por lo que me trae la consulta.

* Me gustaría que los botones de "Add" fueran un poco más interactivos. En un futuro me gustaría agregar notificaciones flotantes de que las acciones se realizaron con éxito.

* Algunos componentes quedaron con lógica que me parece podría ir en otro componente por separado (como el del score general de un lugar). Estaría bueno hacer otro análisis sobre cómo estructurar mi aplicación.

* Como desde la consola de Firebase no se pueden setear display names para los usuarios, decidí mostrar sus correos para ver de quien era cada review. Esto no es ideal, y en futuro estaría bueno poder arreglarlo.

* En los detalles de un lugar dentro de la app, no logre mostrar bien al usuario que la foto que se ve arriba en realidad es un carousel que se puede mover, causando que el mismo paresca una imágen estatica mal alineada nada mal.

* No pude llegar a implementar el upload de imágenes en la app a la hora de agregar un review. Tuve problemas a la hora de subir al storage de firebase, y a la hora de volver al mismo componente en el que estaba luego de sacar una foto con [React Native Image Picker](https://github.com/react-community/react-native-image-picker). Sin embargo, mañana tengo parcial y todavía no pude estudiar, asi que prefiero concentrarme en eso ahora. Dejé el evento para que se abriera la cámara y se pueda sacar una foto, pero luego de sacar una foto no se sube, solo se crea un review de la misma forma que en el sitio web.

## Fuente de información e imágenes

* Google imágenes
* [Trip Advisor](https://www.tripadvisor.com/)
* [Airbnb - things to do](https://www.airbnb.com/things-to-do)