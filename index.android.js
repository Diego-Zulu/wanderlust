/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View
} from 'react-native';
import configureStore from './src/configureStore';
import Root from './src/components/Root';
import { startListeningToAuth } from './src/actions';

const store = configureStore();

export default class Wanderlust extends Component {
  render() {
    return (
      <Root store={store} />
    );
  }
}

AppRegistry.registerComponent('Wanderlust', () => Wanderlust);

setTimeout(function(){
    store.dispatch( startListeningToAuth() );
});