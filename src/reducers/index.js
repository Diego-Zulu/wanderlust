import { combineReducers } from 'redux';

const byId = (state = {}, action = {}) => {
  if(action.response) {
    return {
      ...state,
      ...action.response.entities.places,
    };
  }
  return state;
};

const createList = () => {
  return (state = [], action = {}) => {
    switch (action.type) {
      case 'RECEIVE_PLACES':
        return action.response.result;
      default:
        return state;
    }
  }
};

const createUserTripList = () => {
  return (state = [], action = {}) => {
    switch (action.type) {
      case 'RECEIVE_PLACES_FOR_TRIP':
        return action.response.result;
      default:
        return state;
    }
  }
};

const signIn = (currentstate = {}, action = {}) => {
  switch(action.type){
    case 'ATTEMPTING_LOGIN':
      return {
        currently: 'AWAITING_AUTH_RESPONSE',
        username: "guest",
        uid: null
      };
    case 'LOGOUT':
      return {
        currently: 'ANONYMOUS',
        username: "guest",
        uid: null
      };
    case 'LOGIN_USER':
      return {
        currently: 'LOGGED_IN',
        username: action.username,
        uid: action.uid
      };
    default: return currentstate;
  }
};

const places = combineReducers({
  byId,
  places : createList(), 
  userplaces: createUserTripList(),
  signIn
});

export default places;

export const getVisiblePlaces = (state) => {
  const ids = state.places;
  return ids.map(id => state.byId[id]);
};

export const getPlaceInfoById = (state, id) => {
  
  if (!(id in state.byId)) {
  	return {name: ""};
  }

  return state.byId[id];
};

export const getTripPlacesForUser = (state, userid) => {

  const ids = state.userplaces;
  return ids.map(id => state.byId[id]);
}

export const getSignedInUser = (state) => {

  return state.signIn;
}



