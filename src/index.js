import React from 'react';
import { render } from 'react-dom';
import configureStore from './configureStore';
import Root from './components/Root';
import { startListeningToAuth } from './actions';

const store = configureStore();

render(
  <Root store={store} />,
  document.getElementById('root')
);

setTimeout(function(){
    store.dispatch( startListeningToAuth() );
});
