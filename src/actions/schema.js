import { schema } from 'normalizr';

export const place = new schema.Entity('places');

export const arrayOfPlaces = new schema.Array(place);

