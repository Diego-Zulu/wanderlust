import * as api from '../api';
import { normalize } from 'normalizr';
import * as schema from './schema';
import {firebaseAuth} from '../api/firebase';

const requestPlaces = () => {
  return {
    type: 'REQUEST_PLACES'
  }
}
const receivePlaces = (filter, response) => {
  return {
    type: 'RECEIVE_PLACES',
    filter,
    response
  }
}

const startAddingPlaceToTrip = (userID, placeID) => {
  return {
    type: 'ADD_PLACE_TO_TRIP',
    userID,
    placeID
  }
}
const addingPlaceFinished = () => {
  return {
    type: 'FINISHED_ADDING_PLACE_TO_TRIP',
  }
}
const addingPlaceFailed = (error) => {
  return {
    type: 'FAILED_ADDING_PLACE_TO_TRIP',
    error
  }
}

const tryingToDeletingPlaceFromTrip = (userID, placeID) => {
  return {
    type: 'DELETE_PLACE_FROM_TRIP',
    userID,
    placeID
  }
}

const deletingFromTripFinished = () => {
  return {
    type: 'FINISHED_DELETING_PLACE_FROM_TRIP',
  }
}

const deletingPlaceFromTripFailed = (error) => {
  return {
    type: 'FAILED_DELETING_PLACE_FROM_TRIP',
    error
  }
}

export const addReview = (username, userID, placeID, text, positiveVote, image = null) => (dispatch) =>

  api.addReview(username, userID, placeID, text, positiveVote, image).then(() => {
    dispatch({
      type: 'ADD_REVIEW_SUCCESS'
    });
    dispatch(forceFetchPlaces('all'));
  });

export function addToTrip(userID, placeID) {
  return function(dispatch) {
    dispatch(startAddingPlaceToTrip(userID, placeID))
    return api.addToTrip(userID, placeID).then(() => dispatch(addingPlaceFinished())).catch((error) => {
      console.log(error);
      dispatch(addingPlaceFailed(error));
    });
  }
}

export function deleteFromTrip(userID, placeID) {
  return function(dispatch) {
    dispatch(tryingToDeletingPlaceFromTrip(userID, placeID))
    return api.deleteFromTrip(userID, placeID).then(() => {
      dispatch(deletingFromTripFinished())
      dispatch(fetchPlacesFromTrip(userID));
      }).catch((error) => {
      console.log(error);
      dispatch(deletingPlaceFromTripFailed(error));
    });
  }
}

export function fetchPlaces(filter) {
  return function (dispatch) {
    dispatch(requestPlaces())
    return api.fetchPlaces(filter)
      .then(places => {
        dispatch(receivePlaces(filter, normalize(places, schema.arrayOfPlaces)));
      });
  }
}

export function forceFetchPlaces(filter) {
  return function (dispatch) {
    dispatch(requestPlaces())
    return api.forceFetchPlaces(filter)
      .then(places => {
        dispatch(receivePlaces(filter, normalize(places, schema.arrayOfPlaces)));
      });
  }
}

const requestPlacesForTrip = () => {
  return {
    type: 'REQUEST_PLACES_FOR_TRIP'
  }
}
const receivePlacesForTrip = (userID, response) => {
  return {
    type: 'RECEIVE_PLACES_FOR_TRIP',
    userID,
    response
  }
}

export function fetchPlacesFromTrip(userID) {
  return function (dispatch) {
    dispatch(requestPlacesForTrip())
    return api.fetchPlacesFromUserTrip(userID)
      .then(places => {
        dispatch(receivePlacesForTrip(userID, normalize(places, schema.arrayOfPlaces)));
      });
  }
}

const reorderingTripList = () => {
  return {
    type: 'REORDERING_TRIP_LIST'
  }
}

export function reOrderTripList(userID, lastOrder, newOrder) {
  return function (dispatch) {
    dispatch(reorderingTripList())
    return api.reOrderTripPlaces(userID, lastOrder, newOrder)
      .then(() => {
       dispatch(fetchPlacesFromTrip(userID));
      });
  }
}

export function startListeningToAuth() {
    return function(dispatch,getState){
      firebaseAuth.onAuthStateChanged(function(authData){
        if (authData){ 
          dispatch({
            type: 'LOGIN_USER',
            uid: authData.uid,
            username: authData.email
          });
        } else {
          if (getState().auth && getState().auth.currently !== 'ANONYMOUS'){ 
            dispatch({type:'LOGOUT'});
          }
        }
      });
    }
}

export function attemptLogin(email, password){
    return function(dispatch){
      dispatch({type:'ATTEMPTING_LOGIN'});
      firebaseAuth.signInWithEmailAndPassword(email, password)
          .catch(function(error) {

        var errorCode = error.code;
        var errorMessage = error.message;
        if (errorCode === 'auth/wrong-password') {
          alert('Wrong password.');
        } else {
          alert(errorMessage);
        }
        dispatch({type:'LOGOUT'});
        console.log(error);
      });
    }
}

export function logoutUser(){
    return function(dispatch){
      dispatch({type:'LOGOUT'}); 
      firebaseAuth.signOut();
    }
  }


