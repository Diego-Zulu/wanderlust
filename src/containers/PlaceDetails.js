import React, { Component } from 'react';
import { connect } from 'react-redux';
import { addToTrip, forceFetchPlaces } from '../actions';
import { getPlaceInfoById, getSignedInUser } from '../reducers';

export const container = T => class PlaceDetails extends Component {
	componentDidMount() {
    	this.props.fetchPlaces();
  	}

	render() {	
    return <T {...this.props} />
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    getTargetPlace: (id) => getPlaceInfoById(
      state, id
    ),
    auth: getSignedInUser(
      state
    )
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onAddToTripClick: (userid, placeid) => {
      dispatch(addToTrip(userid, placeid));
    },
    fetchPlaces: () => {
      dispatch(forceFetchPlaces('all'));
    }
  };
};

export const connections = connect(
  mapStateToProps,
  mapDispatchToProps
);