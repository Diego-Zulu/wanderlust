import React, { Component } from 'react'
import { connect } from 'react-redux';
import { getTripPlacesForUser, getSignedInUser } from '../reducers';
import { deleteFromTrip, fetchPlacesFromTrip, reOrderTripList, forceFetchPlaces } from '../actions';

export const container = T => class MyTrip extends Component {

  fetchData() {
    this.props.fetchPlacesFromTrip(this.props.auth.uid);
  }
  componentWillMount() {
      this.fetchData();
  }
  render() {


      return <T {...this.props} />
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    places: getTripPlacesForUser(
      state, ownProps.userID
    ),
    auth: getSignedInUser(
      state
    ),
    fetchNeeded: false
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onDeleteFromTripClick: (userid, placeid) => {
      dispatch(deleteFromTrip(userid, placeid));
    },
    reOrderList: (userid, lastOrder, newOrder) => {
      dispatch(reOrderTripList(userid, lastOrder, newOrder));
    },
    fetchPlacesFromTrip: (userid) => {
      dispatch(fetchPlacesFromTrip(userid));
    },
    fetchAllPlaces: () => {
      dispatch(forceFetchPlaces('all'));
    }
  };
};

export const connections = connect(
  mapStateToProps,
  mapDispatchToProps
);
