import { connect } from 'react-redux';
import {logoutUser, attemptLogin} from '../actions';
import { getSignedInUser } from '../reducers';

var mapStateToProps = function(state, ownProps){

  return {
    auth: getSignedInUser(
      state
    ),
  };
};

var mapDispatchToProps = function(dispatch){
  return {
    attemptLogin: (user, pass) => {
      dispatch(attemptLogin(user, pass));
    },
    logoutUser: () => {
      dispatch(logoutUser());
    },
  };
};

export const connections = connect(
  mapStateToProps,
  mapDispatchToProps
);