import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getSignedInUser } from '../../reducers';

export const container = T => class ReviewsManager extends Component {
	countVotes = reviews => {
      let positiveVotes = 0;
      let negativeVotes = 0;
      for (let oneReview in reviews) {
          if (reviews[oneReview]['positiveVote']) {
            positiveVotes++;
          } else {
            negativeVotes++;
          }
      }
      return {positiveVotes, negativeVotes};
  	}
	render() {

    return <T {...this.props} 
            countVotes={this.countVotes}/>
	}
}

const mapStateToProps = (state, ownProps) => {
  return {
    auth: getSignedInUser(
      state
    )
  };
};

export const connections = connect(
  mapStateToProps
);

