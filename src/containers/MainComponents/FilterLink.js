import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import fetchPlaces from '../../actions'

const mapDispatchToProps = dispatch => ({
  fetchPlaces: bindActionCreators(fetchPlaces, dispatch)
})

export default connect(
  mapDispatchToProps
)