import { connect } from 'react-redux'
import {getSignedInUser} from '../../reducers'

const mapStateToProps = (state, ownProps) => {
  return {
    auth: getSignedInUser(
      state
    )
  };
};

export default connect(
  mapStateToProps
)