import React, { Component } from 'react';
import {
  View, 
  TextInput,
  Button,
  StyleSheet
} from 'react-native'
import {connections }from '../../containers/AuthPanel'

class Authpanel extends Component {

  state = {
    userName: '',
    userPass: ''
  }

  handleChangeUserName = userName => this.setState({ userName })

  handleChangeUserPass = userPass => this.setState({ userPass })

  render() {
    const { navigate } = this.props.navigation;
  	let p = this.props, auth = p.auth === undefined ? "" : p.auth;
    let usernameTripTitle = `${auth.username}'s Trip`;
    switch(auth.currently){
      case 'LOGGED_IN': 
      return (
        <View style={styles.view}>
      		<Button color="#5bc0de" title={usernameTripTitle} onPress={() => navigate('MyTrip')} />
          <Button color='#f0ad4e' onPress={() => p.logoutUser()} title="Logout" />
          </View>
      );
      case 'AWAITING_AUTH_RESPONSE': return (

          <Button disabled title="authenticating..." />

      );
      default: return (
        <View style={styles.view}> 

    	<TextInput
        placeholder='Email'
        value={this.state.userName}
        onChangeText={this.handleChangeUserName}
        keyboardType='email-address' /> 

        <TextInput
        placeholder='Pass'
        value={this.state.userPass}
        onChangeText={this.handleChangeUserPass}
        secureTextEntry={true} />

        <Button color='#0275d8' onPress={() => 
          {if (!this.state.userName.trim() || !this.state.userPass.trim()) {
            return;
          }
          p.attemptLogin(this.state.userName, this.state.userPass);
          this.setState({ userPass: '',  userName: ''});
      	}} title="Login" />

        </View>
      );
    }
    }
};

export default connections(Authpanel);

const styles = StyleSheet.create({
  title: {
    fontWeight: 'bold',
    textAlign: 'center',
    fontSize: 20,
    padding: 8,
    marginLeft: 16,
    marginRight: 16,
  },
  image: {
    borderRadius: 1,
    padding: 8,
    height:300, width: 300,
    marginBottom: 8,
  },
  view: {
    padding: 8,
  }
})