import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Navbar, Nav} from 'react-bootstrap';
import styled from 'styled-components';
import {connections }from '../../containers/AuthPanel'
import { Button, FormGroup,  FormControl, Form} from 'react-bootstrap';

const StyledLink = styled(Link)`

  &:link {
    color: grey;
    text-decoration: none;
  }

  &:visited {
    color: grey;
    text-decoration: none;
  }

  &:hover {
    color: white;
    text-decoration: none;
  }

  &:active {
    color: white;
    text-decoration: none;
  }
`;

const MiddleForm = styled(Form)`
  margin-top:8px;
  margin-right:15px;
`;

class Authpanel extends Component {


  render() {
    let passwordInput;
    let userInput;
    let p = this.props, auth = p.auth === undefined ? "" : p.auth;
    switch(auth.currently){
      case 'LOGGED_IN': 
      return (
        <Nav pullRight>
          <Navbar.Text>
            <StyledLink to={"/user/my-trip/"}>{auth.username}'s Trip</StyledLink>
          </Navbar.Text>
          <Navbar.Text>
            <StyledLink to={"#"} onClick={() => p.logoutUser()}>Logout</StyledLink>
          </Navbar.Text>
        </Nav>
      );
      case 'AWAITING_AUTH_RESPONSE': return (
        <Nav pullRight>
          <button disabled><i className="fa fa-spinner fa-spin"></i> authenticating...</button>
        </Nav>
      );
      default: return (
        <Nav pullRight>
        <MiddleForm inline
        onSubmit={e => {
          e.preventDefault();
          if (!userInput.value.trim() || !passwordInput.value.trim()) {
            return;
          }
          p.attemptLogin(userInput.value, passwordInput.value);
          userInput.value = '';
          passwordInput.value = '';
        }}
      >
          <FormGroup>
              <FormControl inputRef={node => { userInput = node;}} type="email" placeholder="Username"/>
            </FormGroup>
            <FormGroup>
              <FormControl inputRef={node => { passwordInput = node;}} type="password" placeholder="Password"/>
            </FormGroup>
              <Button bsStyle="primary" type="submit">Sign in!</Button>
        </MiddleForm>
        </Nav>
      );
    }
  }
};

export default connections(Authpanel);