import React from 'react';
import { ListGroupItem } from 'react-bootstrap';

const Activity = ({description, name, id, price}) => (
  <ListGroupItem>
    <h4>{name}<small className="text-muted"><p>{description}</p></small></h4>
    <h5>Price: <span className="text-muted">{price}</span></h5>
  </ListGroupItem>
);

export default Activity;