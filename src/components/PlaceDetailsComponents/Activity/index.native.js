import React from 'react';
import {
  View, 
  Text,
  StyleSheet
} from 'react-native'

const Activity = ({description, name, id, price}) => (
  <View style={styles.view}>
    <Text><Text style={styles.bold}>{name}: </Text>{description}</Text>
    <Text><Text style={styles.underline}>Price: </Text>{price}</Text>
  </View>
);

export default Activity;

const styles = StyleSheet.create({
  bold: {
    fontWeight: 'bold'
  },
  underline: {
    textDecorationLine: 'underline'
},
view: {
	marginBottom: 8
}
})