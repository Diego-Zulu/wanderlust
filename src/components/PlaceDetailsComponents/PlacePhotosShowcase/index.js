import React from 'react';
import { Image, Carousel } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

const StyledLink = styled(Link)`

  opacity: 0.8;
  background-color: #101010;

  &:link {
    color: grey;
    text-decoration: none;
  }

  &:visited {
    color: grey;
    text-decoration: none;
  }

  &:hover {
    color: white;
    text-decoration: none;
  }

  &:active {
    color: white;
    text-decoration: none;
  }
`;

const PlacePhotosShowcase = ({photos, altForPhotos}) => {

  let vrPhotos = [];
  let normalPhotos = [];
  for (let photo in photos) {

    if (photos[photo]['vr']) {
      vrPhotos.push(photos[photo]);
    } else {
      normalPhotos.push(photos[photo]);
    }
  }

  return (

      <div>
    
        <Carousel>
        {normalPhotos.map(photo => <Carousel.Item key={photo.id}><Image className="center-block" src={photo.link} 
          alt={altForPhotos} responsive thumbnail/></Carousel.Item>)}
        {vrPhotos.map(photo => <Carousel.Item key={photo.id}><Image className="center-block" src={photo.link} 
          alt={altForPhotos} responsive thumbnail/><Carousel.Caption >
            <StyledLink to={photo.vr_link} target="_blank" > Ver en VR </StyledLink></Carousel.Caption></Carousel.Item>)}
        </Carousel>

      </div>


)};

export default PlacePhotosShowcase;