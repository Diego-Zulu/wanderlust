import React from 'react';
import { Image, StyleSheet, View} from 'react-native';
import Carousel from 'react-native-snap-carousel';

const PlacePhotosShowcase = ({photos, altForPhotos}) => {

	const actualPhotos = photos === undefined ? [] : photos;
	const slides = actualPhotos.map((entry, index) => {
            return (
                <Image key={`entry-${index}`} style={styles.image} source={{uri: entry.link}} />
            );
        });

  return (

      <View style= {{justifyContent: 'center', alignItems: 'center' }}>
      <Carousel
              ref={(carousel) => { this._carousel = carousel; }}
              sliderWidth={300}
              itemWidth={300}
            >
              {slides}
            </Carousel>

        </View>
)};

export default PlacePhotosShowcase;

const styles = StyleSheet.create({
  image: {
    height:300, width: 300,
    marginBottom: 8,
  }
})