import React from 'react';
import Review from '../Review';
import { Accordion } from 'react-bootstrap';

const ReviewsList = ({reviews}) => {

  let noReviewAvailable = "";
  var reviewsList = [];
  for (let oneReview in reviews) {
    reviewsList.push(reviews[oneReview]);

  }
  if (reviewsList.length === 0) {
    noReviewAvailable = <h4> No reviews ... yet </h4>;
  }

  return (
    <div> 
        <Accordion>
        {reviewsList.map(review =>
          <Review
            key={review.id}
            {...review}
          />
        )}
        </Accordion>
        {noReviewAvailable}
    </div>

)};

export default ReviewsList;
  