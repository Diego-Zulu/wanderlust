import React, { Component } from 'react';
import Review from '../Review';
import {
  ListView,
  Text,
  View,
  StyleSheet
} from 'react-native'

class ReviewsList extends Component {

  render() {
    let reviews = this.props.reviews;
    let reviewsList = [];
    for (let oneReview in reviews) {
      reviewsList.push(reviews[oneReview]);
    }
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

    return (
    <View>
      <ListView
        dataSource={ds.cloneWithRows(reviewsList)}
        renderRow={review =>
            <Review
              key={review.id}
              {...review}
            />
          } />
          {reviewsList.length === 0 ? <Text style={styles.center}> No reviews ... yet </Text>  : null}
      </View>

  )}
};

export default ReviewsList;

const styles = StyleSheet.create({
  center: {
    marginLeft: 16,
    marginRight: 16,
    textAlign: 'center',
  }
})