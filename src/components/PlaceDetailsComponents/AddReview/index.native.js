import React, {Component} from 'react';
import { connect } from 'react-redux';
import { addReview } from '../../../actions/index';
import {
  AppRegistry,
  StyleSheet,
  Text,
  Image,
  Platform,
  Button,
  View,
  TextInput
} from 'react-native';
import { SegmentedControls } from 'react-native-radio-buttons'
import RNFetchBlob from 'react-native-fetch-blob'
import {firebaseStorage} from '../../../api/firebase';
var ImagePicker = require('react-native-image-picker');

var options = {
};


const Blob = RNFetchBlob.polyfill.Blob
const fs = RNFetchBlob.fs
window.XMLHttpRequest = RNFetchBlob.polyfill.XMLHttpRequest
window.Blob = Blob


class AddReview extends Component {

	uploadImage(uri, mime = 'application/octet-stream') {
    return new Promise((resolve, reject) => {
      const uploadUri = Platform.OS === 'ios' ? uri.replace('file://', '') : uri
      let uploadBlob = null

      const imageRef = firebaseStorage.ref('images').child({uploadUri})

      fs.readFile(uploadUri, 'base64')
        .then((data) => {
          return Blob.build(data, { type: `${mime};BASE64` })
        })
        .then((blob) => {
          uploadBlob = blob
          return imageRef.put(blob, { contentType: mime })
        })
        .then(() => {
          uploadBlob === null ? reject("null blob") : uploadBlob.close()
          return imageRef.getDownloadURL()
        })
        .catch((error) => {
          reject("segundo error")
      })
    })
  }

	startUploadProcess(){

		const {username, userID, placeID, addNewReview} = this.props;
    	addNewReview(username, userID, placeID, this.state.review, this.state.vote === '+' ? true : false);
       this.setState({ review: '',  vote: '+'});
    ImagePicker.launchCamera(options, (response)  => {
        
       //ACA IRIA EL UPLOAD DE LA FOTO, PERO NO LO PUDE HACER FUNCAR
      /*if (response.didCancel) {
        console.warn('User cancelled image picker');
      }
      else if (response.error) {
        console.warn('ImagePicker Error');
      }
      else {




      this.uploadImage(response.uri)
        .then(url => {
        	console.warn("funco")})
        .catch(error => console.warn(error))

      }*/
    });

  }

  state = {
    review: '',
    vote: '+'
  }

  handleChangeReview = review => this.setState({ review })

  handleChangeVote = vote => this.setState({ vote })

  render () {
  	
  return (
    <View> 
    	<Text style={styles.title}> Add or Update Review </Text>
    	<TextInput
        placeholder='Enter comment'
        value={this.state.review}
        onChangeText={this.handleChangeReview}/> 

    <SegmentedControls
	  options={ ['+', '-'] }
	  onSelection={this.handleChangeVote}
	  selectedOption={ this.state.vote }/>

        <Button style={styles.button} color='#0275d8' onPress={() => 
          {if (!this.state.review.trim()) {
            return;
          }
          this.startUploadProcess();
      	}} title="Do it!" />

        </View>
  	);
	}
};

var mapDispatchToProps = function(dispatch){
  return {
    addNewReview: (username, userID, placeID, reviewText, voteValue, image) => {
      dispatch(addReview(username, userID, placeID, reviewText, voteValue, image));
    }
  };
};

AddReview = connect(null, 
  mapDispatchToProps
)(AddReview);

export default AddReview;

const styles = StyleSheet.create({
  title: {
    fontWeight: 'bold',
    textAlign: 'center',
    fontSize: 20,
    padding: 8,
    marginLeft: 16,
    marginRight: 16,
  },
  button: {
    marginTop: 16
  }
})
