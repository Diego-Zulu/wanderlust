import React from 'react';
import { addReview } from '../../../actions/index';
import { Button, Radio, FormGroup, ControlLabel,  FormControl, Image} from 'react-bootstrap';

const AddReview = ({ dispatch, username, userID, placeID}) => {
  let input;
  let voteUpInput;

  return (
    <div>
      <form
        onSubmit={e => {
          e.preventDefault();
          if (!input.value.trim()) {
            return;
          }
          dispatch(addReview(username, userID, placeID, input.value, voteUpInput.checked));
          input.value = '';
        }}
      >
        <FormGroup>
          <ControlLabel><h4> Add or Update Review </h4></ControlLabel>
          <FormControl inputRef={node => { input = node;}} componentClass="textarea" placeholder="Enter comment"/>
        </FormGroup>
        <FormGroup>
          <Radio inputRef={node => { voteUpInput = node; if (voteUpInput !== null) voteUpInput.checked = true;}} name="vote" value="true" inline> 
            <Image alt="vote up" src="https://firebasestorage.googleapis.com/v0/b/wanderlust-backend.appspot.com/o/ic_thumb_up_black_24dp_1x.png?alt=media&token=a094f892-9359-436f-94b5-1557c8245cb4"/> </Radio>
          <Radio name="vote" value="false" inline> 
            <Image alt="vote down" src="https://firebasestorage.googleapis.com/v0/b/wanderlust-backend.appspot.com/o/ic_thumb_down_black_24dp_1x.png?alt=media&token=ed4caa1c-5791-4789-8b01-949cdaf22e4e" /> </Radio>
          {" "}<Button bsStyle="primary" type="submit">Do it!</Button>
        </FormGroup>
      </form>
    </div>
  );
};

export default (AddReview);
