import React, { Component } from 'react';
import Activity from '../Activity';
import {
  ListView,
  View, 
  Text,
  StyleSheet
} from 'react-native'

class ActivityList extends Component {

  render() {
  let activities = this.props.activities;
  const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

  return (

    <View><Text style={styles.title}>Activities:</Text>
    
    {activities === undefined || activities.length === 0 ? <Text style={styles.center}> No activities ... yet </Text> : 
      <ListView
        dataSource={ds.cloneWithRows(activities)}
        renderRow={activ =>
      <Activity
        key={activ.name}
        {...activ}
      />
    }/>}
  </View>
  )}
};

export default ActivityList;

const styles = StyleSheet.create({
  title: {
    fontWeight: 'bold',
    textAlign: 'center',
    fontSize: 20,
    padding: 8,
    marginLeft: 16,
    marginRight: 16,
  },
  subtitle: {
    fontWeight: 'bold',
    textAlign: 'center',
    fontSize: 14,
    padding: 8,
    marginLeft: 16,
    marginRight: 16,},
  image: {
    borderRadius: 1,
    padding: 8,
    height:300, width: 300,

  },
  sectionTitle: {
    fontWeight: 'bold',
  },
  button: {
    marginLeft: 8,
    marginRight: 8,
  },
  center: {
    marginLeft: 16,
    marginRight: 16,
    textAlign: 'center',
  }
})