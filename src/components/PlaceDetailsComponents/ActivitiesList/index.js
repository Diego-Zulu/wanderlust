import React from 'react';
import Activity from '../Activity';
import { Panel, ListGroup } from 'react-bootstrap';
import styled from 'styled-components';

const PanelWithPointer = styled(Panel)`
  cursor: pointer;
`;

const ListGroupWithAutoCursor = styled(ListGroup)`
  cursor: auto;
`;

const ActivityList = ({activities}) => {

  let noActivityAvailable = "";
  if (activities === undefined || activities.length === 0) {
    noActivityAvailable = <h4> No activities ... yet </h4>;
  }
  return (

    <PanelWithPointer bsStyle="primary" collapsible defaultExpanded header="Activities:">
    <ListGroupWithAutoCursor fill>
    {activities.map(activ =>
      <Activity
        key={activ.name}
        {...activ}
      />
    )}
    {noActivityAvailable}
  </ListGroupWithAutoCursor>
  </PanelWithPointer>
)};

export default ActivityList;