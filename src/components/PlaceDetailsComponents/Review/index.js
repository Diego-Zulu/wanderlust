import React from 'react';
import { Panel, Image } from 'react-bootstrap';
import styled from 'styled-components';

const PanelWithPointer = styled(Panel)`
  cursor: pointer;
`;

const Review = ({username, content, positiveVote, id}) => {

  let panelHeader = `Review by: ${username}`;
  return (
  <PanelWithPointer collapsible header={panelHeader} eventKey={id}>
    <div style={{cursor:'auto'}}>
      <h4>"{content}"</h4>
      <h5>Vote: <Image alt="user vote" src={positiveVote ? "https://firebasestorage.googleapis.com/v0/b/wanderlust-backend.appspot.com/o/ic_thumb_up_black_24dp_1x.png?alt=media&token=a094f892-9359-436f-94b5-1557c8245cb4" :
		  "https://firebasestorage.googleapis.com/v0/b/wanderlust-backend.appspot.com/o/ic_thumb_down_black_24dp_1x.png?alt=media&token=ed4caa1c-5791-4789-8b01-949cdaf22e4e"} /></h5>
    </div>
  </PanelWithPointer>
)};

export default Review;