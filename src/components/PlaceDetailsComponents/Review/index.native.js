import React from 'react';
import {
  View,
  Image,
  Text,
  StyleSheet
} from 'react-native'

const Review = ({username, content, positiveVote, id}) => {

  let panelHeader = `Review by: ${username}`;
  return (
  <View style={styles.view}>
      <Text style={styles.center}>{panelHeader}</Text>
      <Text style={styles.center}>"{content}"</Text>
      <Text style={styles.center}>Vote: </Text>
      <View style= {{justifyContent: 'center', alignItems: 'center' }}><Image
      style= {{ height:24, width: 24}}
          source={{uri: positiveVote ? "https://firebasestorage.googleapis.com/v0/b/wanderlust-backend.appspot.com/o/ic_thumb_up_black_24dp_1x.png?alt=media&token=a094f892-9359-436f-94b5-1557c8245cb4" :
		  "https://firebasestorage.googleapis.com/v0/b/wanderlust-backend.appspot.com/o/ic_thumb_down_black_24dp_1x.png?alt=media&token=ed4caa1c-5791-4789-8b01-949cdaf22e4e"}} /></View>
  </View>
)};

export default Review;

const styles = StyleSheet.create({
  title: {
    fontWeight: 'bold',
    textAlign: 'center',
    fontSize: 20,
    padding: 8,
    marginLeft: 16,
    marginRight: 16,
  },
  subtitle: {
    fontWeight: 'bold',
    textAlign: 'center',
    fontSize: 14,
    padding: 8,
    marginLeft: 16,
    marginRight: 16,
  },
  center: {
    marginLeft: 16,
    marginRight: 16,
    textAlign: 'center',
  },
  sectionTitle: {
    fontWeight: 'bold',
    marginLeft: 16,
    marginRight: 16,
  },
  view: {
    marginBottom: 8
  }
})