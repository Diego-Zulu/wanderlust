import React, { Component } from 'react';
import ReviewsList from '../ReviewsList';
import AddReview from '../AddReview';
import { Image, ListGroup, Panel, ListGroupItem } from 'react-bootstrap';
import styled from 'styled-components';
import {container, connections} from '../../../containers/PlaceDetailsComponents/ReviewsManager'

const PanelWithPointer = styled(Panel)`
  cursor: pointer;
`;

const ListGroupWithAutoCursor = styled(ListGroup)`
  cursor: auto;
`;

class ReviewsManager extends Component {
  render() {

    let votingResults = this.props.countVotes(this.props.reviews);
    let p = this.props, auth = p.auth === undefined ? "" : p.auth;
    let addReviewForm  = "";
    if (p.auth.uid) {
     addReviewForm = <ListGroupItem> <AddReview userID={auth.uid} username={auth.username} placeID={this.props.placeID}/>  </ListGroupItem>
    }
    return (<PanelWithPointer bsStyle="primary" collapsible defaultExpanded header="Reviews:">
  <ListGroupWithAutoCursor fill>
    <ListGroupItem>
    <h4>Score: <Image alt="total votes" src="https://firebasestorage.googleapis.com/v0/b/wanderlust-backend.appspot.com/o/ic_thumbs_up_down_black_24dp_1x.png?alt=media&token=6b78c105-9c2b-4de6-8139-c43a7a9d29db" /> 
           {" "}{votingResults.positiveVotes}/{votingResults.negativeVotes} </h4>
      </ListGroupItem>
      <ListGroupItem>
        <ReviewsList reviews={this.props.reviews} />
      </ListGroupItem>
      {addReviewForm}
    </ListGroupWithAutoCursor>
  </PanelWithPointer>);
  }
}

export default connections(container(ReviewsManager));



