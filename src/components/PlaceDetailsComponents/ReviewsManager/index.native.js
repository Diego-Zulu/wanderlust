import React, { Component } from 'react';
import ReviewsList from '../ReviewsList';
import AddReview from '../AddReview';
import {container, connections} from '../../../containers/PlaceDetailsComponents/ReviewsManager'
import {
  View, 
  Text,
  Image,
  StyleSheet
} from 'react-native'

class ReviewsManager extends Component {
  render() {

    let votingResults = this.props.countVotes(this.props.reviews);
    let p = this.props, auth = p.auth === undefined ? "" : p.auth;
    return (<View> 
    <Text style={styles.title}>Reviews:</Text>
    <Text style={styles.sectionTitle}>Score:  </Text>
    <View style= {{justifyContent: 'center' , alignItems: 'center'}}><Image
    style= {{ height:48, width: 48}}
          source={{uri: "https://firebasestorage.googleapis.com/v0/b/wanderlust-backend.appspot.com/o/ic_thumbs_up_down_black_24dp_1x.png?alt=media&token=6b78c105-9c2b-4de6-8139-c43a7a9d29db"}}
        /></View>
           <Text style={styles.sectionTitle}>{votingResults.positiveVotes}/{votingResults.negativeVotes}</Text>
        
        <ReviewsList reviews={this.props.reviews} />


      {p.auth.uid ? <AddReview userID={auth.uid} username={auth.username} placeID={this.props.placeID}/>  : null}
  </View>);
  }
}

export default connections(container(ReviewsManager));

const styles = StyleSheet.create({
  title: {
    fontWeight: 'bold',
    textAlign: 'center',
    fontSize: 20,
    padding: 8,
    marginLeft: 16,
    marginRight: 16,
  },
  center: {
    marginLeft: 16,
    marginRight: 16,
    textAlign: 'center',
  },
  sectionTitle: {
    fontWeight: 'bold',
    textAlign: 'center',
  },
})



