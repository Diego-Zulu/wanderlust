import React, { Component } from 'react';
import MyTripList from '../MyTripComponents/MyTripList';
import {container, connections }from '../../containers/MyTrip'
import { Left } from 'react-navigation';

class MyTrip extends Component {

  render() {

      return <MyTripList {...this.props} />
  }
}

const MyTripScreen = connections(container(MyTrip));

MyTripScreen.navigationOptions = {
	  title: "My Trip"
};

export default MyTripScreen;