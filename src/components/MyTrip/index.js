import React, { Component } from 'react';
import MyTripList from '../MyTripComponents/MyTripList';
import {container, connections }from '../../containers/MyTrip'
import { Redirect } from 'react-router';

class MyTrip extends Component {

  render() {
    if (!this.props.auth.uid) {

      return <Redirect to="/" />;
    } else {

      return <MyTripList {...this.props} />
    }
  }
}

export default connections(container(MyTrip))