import React from 'react';
import PlaceDetails from '../PlaceDetails';
import MyTrip from '../MyTrip';
import Main from '../Main';
import AuthPanel from '../AuthPanel';
import { Route } from 'react-router-dom';
import { Link } from 'react-router-dom';
import { Navbar} from 'react-bootstrap';

const Layout = () => (
  <div>
  <Navbar inverse collapseOnSelect fluid staticTop>
    <Navbar.Header>
      <Navbar.Brand>
        <Link to={"/"}>Wanderlust</Link>
      </Navbar.Brand>
      <Navbar.Toggle />
    </Navbar.Header>
    <Navbar.Collapse>
      <AuthPanel />
    </Navbar.Collapse>
  </Navbar>
      <div className="text-center center-block"> 
		    <Route exact path='/:filter?' component={Main}></Route>
		    <Route exact path='/places/:placeID' component={PlaceDetails}></Route>
		   	<Route exact path='/user/my-trip/' component={MyTrip}></Route>
      </div>
  </div>
);

export default Layout;

