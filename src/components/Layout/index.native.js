import React from 'react';
import PlaceDetails from '../PlaceDetails';
import MyTrip from '../MyTrip';
import Main from '../Main';
import AuthPanel from '../AuthPanel';
import { TabNavigator, StackNavigator } from 'react-navigation';
import {Text} from 'react-native';

const Layout = TabNavigator({
  Main: {screen: Main},
  Profile: {screen: AuthPanel},
},{
  tabBarOptions: {
    activeTintColor: 'white',
    inactiveTintColor: '#c8c8c8',
    indicatorStyle: {
    	backgroundColor: '#0275d8'
    },
    style: {
    	backgroundColor: '#3c3c3c',
  	}
  }});

const MainStack = StackNavigator({
  Main: {screen: Layout},
  More: {screen: PlaceDetails},
  MyTrip: {screen: MyTrip}
});

Layout.navigationOptions = {
  title: 'Wanderlust',
  headerTitleStyle: {
      color: '#9d9d9d',
   },
   headerStyle: {
   	  backgroundColor: '#222222'
   },
};



export default () => <MainStack />;

