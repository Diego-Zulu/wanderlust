import React from 'react';
import { Link } from 'react-router-dom';
import {Button, Col, Thumbnail, Image} from 'react-bootstrap';
import styled from 'styled-components';

const ImageWithPointerCursor = styled(Image)`
  cursor: pointer;
`;

const Place = ({onDeleteFromTripClick, reOrderList, photos, name, id, order}) => (
  <Col lg={3} md={4} xs={6}>
      <Thumbnail src={photos[0].link} alt={name + " photo"}>
        <h3>{order}.{name}</h3>
        <p>
          <ImageWithPointerCursor alt="Move up" onClick={() => reOrderList(order-1, order)} src="https://firebasestorage.googleapis.com/v0/b/wanderlust-backend.appspot.com/o/ic_arrow_upward_black_24dp_2x.png?alt=media&token=4b9090d4-b8d9-4c2d-8c1d-68dadd61819e" />
          <ImageWithPointerCursor alt="Move down" onClick={() => reOrderList(order, order+1)} src="https://firebasestorage.googleapis.com/v0/b/wanderlust-backend.appspot.com/o/ic_arrow_downward_black_24dp_2x.png?alt=media&token=cf295a0d-b04e-4b58-a4b0-68d3f739b9f9" />
        </p>
        <p>
        <Link to={"/places/" + id} ><Button bsStyle="info">More</Button></Link>&nbsp;
        <Button bsStyle="danger" onClick={() => onDeleteFromTripClick()}>Remove</Button>
        </p>
      </Thumbnail>
    </Col>
);

export default Place;

