import React from 'react';
import {
  View,
  Image,
  Text,
  Button,
  TouchableHighlight,
  StyleSheet
} from 'react-native'

const Place = ({onDeleteFromTripClick, reOrderList, photos, name, id, order, navigation}) => {

  const { navigate } = navigation;
  return (
  <View style={styles.view}>
      <Text style={styles.title}>{order}.{name}</Text>
      <Image
          source={{uri: photos[0].link}}
          style= {{ height:300, width: 300 }}
        />
        
      <TouchableHighlight onPress={() => reOrderList(order-1, order)}>
        <Image
        style= {{ height:48, width: 48 }}
        source={{uri: "https://firebasestorage.googleapis.com/v0/b/wanderlust-backend.appspot.com/o/ic_arrow_upward_black_24dp_2x.png?alt=media&token=4b9090d4-b8d9-4c2d-8c1d-68dadd61819e"}}
       />
      </TouchableHighlight>

      <TouchableHighlight onPress={() => reOrderList(order, order+1)}>
        <Image
        style= {{ height:48, width: 48 }}
        source={{uri: "https://firebasestorage.googleapis.com/v0/b/wanderlust-backend.appspot.com/o/ic_arrow_downward_black_24dp_2x.png?alt=media&token=cf295a0d-b04e-4b58-a4b0-68d3f739b9f9"}}
       />
      </TouchableHighlight>
        
        <Button color="#5bc0de" title="More" onPress={() => navigate('More', { placeID: id })}/>
        <Button color='#d9534f' title="Remove" onPress={() => onDeleteFromTripClick()}/>
    </View>
)};

export default Place;

const styles = StyleSheet.create({
  title: {
    fontWeight: 'bold',
    textAlign: 'center',
    fontSize: 20,
    padding: 8,
    marginLeft: 16,
    marginRight: 16,
  },
  image: {
    borderRadius: 0.5,
    padding: 8,
    height:300, width: 300,
    marginBottom: 8,
  },
  view: {
    padding: 8,
    justifyContent: 'center',
    alignItems: 'center',
  }
})

