import React, { Component } from 'react';
import TripPlace from '../TripPlace';
import {
  ListView,
  Text,
  View
} from 'react-native'

class MyTripList extends Component {

render() {  
  const {auth, places, onDeleteFromTripClick, reOrderList} = this.props;
  let noPlaceForTripAvailable;
  const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
  return (
  <View>
    <ListView
      dataSource={ds.cloneWithRows(this.props.places)}
      renderRow={place =>
      <TripPlace
        key={place.name}
        {...place}
        navigation={this.props.navigation}
        onDeleteFromTripClick={() => onDeleteFromTripClick(auth.uid, place.id)}
        reOrderList={(lastOrder, newOrder) => reOrderList(auth.uid, lastOrder, newOrder)}
      />}
      />
  {places === undefined || places.length === 0 ? <Text> Add some places first, then come back! </Text> : null}
  </View>
  )}
};

export default MyTripList;