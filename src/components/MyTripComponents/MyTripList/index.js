import React from 'react';
import TripPlace from '../TripPlace';
import { Grid,  Row} from 'react-bootstrap';

const MyTripList = ({ auth, places,  onDeleteFromTripClick, reOrderList}) => {

  let noPlaceForTripAvailable = "";
  if (places === undefined || places.length === 0) {
    noPlaceForTripAvailable = <h4> Add some places first, then come back! </h4>
  }
  return (
  <Grid fluid>
    <Row>
    {places.map(place =>
      <TripPlace
        key={place.name}
        {...place}
        onDeleteFromTripClick={() => onDeleteFromTripClick(auth.uid, place.id)}
        reOrderList={(lastOrder, newOrder) => reOrderList(auth.uid, lastOrder, newOrder)}
      />
    )}
    {noPlaceForTripAvailable}
   </Row>
  </Grid>
)};

export default MyTripList;