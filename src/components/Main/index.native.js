import React, {Component} from 'react';
import VisiblePlaceList from '../MainComponents/VisiblePlaceList';
import Footer from '../MainComponents/Footer';
import {
  Text,
  View
} from 'react-native'

class Main extends Component {

  static navigationOptions = {
    tabBarLabel: 'Home'
  };
  render() {  
    return (

	    <View>
	      <VisiblePlaceList navigation={this.props.navigation} filter={'all'}/>
	    </View>

	)
  }
}

export default Main;
