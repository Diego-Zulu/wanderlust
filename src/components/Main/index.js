import React from 'react';
import VisiblePlaceList from '../MainComponents/VisiblePlaceList';
import Footer from '../MainComponents/Footer';

const Main = (props) => {

	let displayRegionIfAvailable = "";
	if (props.match.params.filter && props.match.params.filter !== 'all') {
		displayRegionIfAvailable = <h3>Region: {props.match.params.filter} </h3>
	} 
	return (

    <div className="text-center">
    	{displayRegionIfAvailable}
      <VisiblePlaceList filter={props.match.params.filter || 'all'}/>
      <Footer />
    </div>

)}

export default Main;
