import React, { Component } from 'react';
import container from '../../../containers/MainComponents/Place'
import {
  View,
  Image,
  Text,
  Button,
    StyleSheet
} from 'react-native'

class Place extends Component {

  render() {
  const { navigate } = this.props.navigation;
  const {photos, name, id, auth, onAddToTripClick} = this.props;
  let addButton;
  if (auth.uid) {
     addButton =  <Button color='#5cb85c' title="Add" onPress={() => onAddToTripClick(auth.uid, id)}/>;
  }
  return (
      <View style= {styles.view}>
      <Text style={styles.title}>{name}</Text>
      <Image
          style= {styles.image}
          source={{uri: photos[0].link}}
        />
        <Button color="#5bc0de" title="More" onPress={() => navigate('More', { placeID: id })}/>
         {addButton}
      </View>
)
  }
}

export default container(Place);

const styles = StyleSheet.create({
  title: {
    fontWeight: 'bold',
    textAlign: 'center',
    fontSize: 20,
    padding: 8,
    marginLeft: 16,
    marginRight: 16,
  },
  image: {
    borderRadius: 0.5,
    padding: 8,
    height:300, width: 300,
    marginBottom: 8,
  },
  view: {
    padding: 8,
    justifyContent: 'center',
    alignItems: 'center',
  }
})