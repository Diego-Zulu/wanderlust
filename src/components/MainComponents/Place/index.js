import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Col, Thumbnail, Button} from 'react-bootstrap';
import container from '../../../containers/MainComponents/Place'

class Place extends Component {

  render() {
  const {onAddToTripClick, photos, name, id, auth} = this.props;
  let addButton = "";
  if (auth.uid) {
     addButton =  <Button bsStyle="success" onClick={() => onAddToTripClick(auth.uid, id)}>Add</Button>;
  }
  return (
  <Col lg={3} md={4} xs={6}>
      <Thumbnail src={photos[0].link} alt={name + " photo"}>
        <h3>{name}</h3>
        <p>
          <Link to={"/places/" + id}> <Button bsStyle="info">More</Button></Link>&nbsp;
         {addButton}
        </p>
      </Thumbnail>
    </Col>
)
  }
}

export default container(Place);