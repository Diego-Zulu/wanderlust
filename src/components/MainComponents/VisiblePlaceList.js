import React, { Component } from 'react';
import { connect } from 'react-redux';
import PlaceList from './PlaceList';
import { getVisiblePlaces } from '../../reducers';
import { addToTrip, fetchPlaces } from '../../actions';

class VisiblePlaceList extends Component {
  componentDidMount() {
    this.fetchData();
  }

  componentDidUpdate(prevProps, prevState) {
    if(this.props.filter !== prevProps.filter){
      this.fetchData();
    }
  }

  fetchData() {
    const { filter } = this.props;
    this.props.fetchPlaces(filter);
  }
  render() {
    return <PlaceList {...this.props} />
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    places: getVisiblePlaces(
      state
    )
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onAddToTripClick: (userid, placeid) => {
      dispatch(addToTrip(userid, placeid));
    },
    fetchPlaces: (filter) => {
      dispatch(fetchPlaces(filter));
    },
  };
};

VisiblePlaceList = connect(
  mapStateToProps,
  mapDispatchToProps
)(VisiblePlaceList);

export default VisiblePlaceList;