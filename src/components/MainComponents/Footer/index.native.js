import React from 'react';
import FilterLink from '../FilterLink';
import {
  View
} from 'react-native'

const Footer = () => (
  <View>
    Show:
    {" "}
    <FilterLink filter="all">
      All
    </FilterLink>
    {", "}
    <FilterLink filter="North America">
      North America
    </FilterLink>
    {", "}
    <FilterLink filter="South America">
      South America
    </FilterLink>
    {", "}
    <FilterLink filter="Africa">
      Africa
    </FilterLink>
    {", "}
    <FilterLink filter="Asia">
      Asia
    </FilterLink>
  </View>
);

export default Footer;