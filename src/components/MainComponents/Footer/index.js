import React from 'react';
import FilterLink from '../FilterLink';

const Footer = () => (
  <p>
    Show:
    {" "}
    <FilterLink filter="all">
      All
    </FilterLink>
    {", "}
    <FilterLink filter="North America">
      North America
    </FilterLink>
    {", "}
    <FilterLink filter="South America">
      South America
    </FilterLink>
    {", "}
    <FilterLink filter="Africa">
      Africa
    </FilterLink>
    {", "}
    <FilterLink filter="Asia">
      Asia
    </FilterLink>
  </p>
);

export default Footer;