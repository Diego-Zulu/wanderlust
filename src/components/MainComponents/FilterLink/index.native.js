import React from 'react';
import { Link } from 'react-router-dom';
import {
  Button
} from 'react-native'
import container from '../../../containers/MainComponents/FilterLink'

const FilterLink = ({ filter, children, fetchPlaces }) => (
  <Button onPress={() => fetchPlaces(filter === 'all' ? '' : filter)}
  	title={filter}
  	color="#841584">
    {children}
  </Button>
);

export default container(FilterLink);