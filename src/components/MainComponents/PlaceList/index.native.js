import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Place from '../Place';
import {
  ListView
} from 'react-native'

class PlaceList extends Component {

  render() {
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    return (
      <ListView
        dataSource={ds.cloneWithRows(this.props.places)}
        renderRow={place =>
      <Place navigation={this.props.navigation}
        key={place.name}
        {...place}
        onAddToTripClick={(uID, pID) => this.props.onAddToTripClick(uID, pID)}
      />}
      />
    );
  }
}

export default PlaceList;
