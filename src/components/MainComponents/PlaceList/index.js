import React from 'react';
import Place from '../Place';
import {Grid,  Row} from 'react-bootstrap';

const PlaceList = ({places,  onAddToTripClick}) => (
  <Grid fluid>
    <Row>
    {places.map(place =>
      <Place
        key={place.name}
        {...place}
        onAddToTripClick={(uID, pID) => onAddToTripClick(uID, pID)}
      />
    )}
    </Row>
  </Grid>
);

export default PlaceList;
