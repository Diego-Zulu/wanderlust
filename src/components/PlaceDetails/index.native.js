import React, { Component } from 'react';
import PlacePhotosShowcase from '../PlaceDetailsComponents/PlacePhotosShowcase';
import ActivitiesList from '../PlaceDetailsComponents/ActivitiesList';
import ReviewsManager from '../PlaceDetailsComponents/ReviewsManager';
import {container, connections }from '../../containers/PlaceDetails'
import {
  View, 
  Text,
  Image,
  Button,
  ScrollView,
  StyleSheet
} from 'react-native'

class PlaceDetails extends Component {

  render() {  
    const { params } = this.props.navigation.state;
    let targetPlace = this.props.getTargetPlace(params.placeID);
    return (<ScrollView>

       <Text style={styles.title}>{targetPlace.name}</Text> 

       {this.props.auth.uid ? <Button color='#5cb85c' style={styles.button} onPress={() => this.props.onAddToTripClick(this.props.auth.uid, targetPlace.id)} title="Add" /> : null}

       <PlacePhotosShowcase altForPhotos={targetPlace.name}  photos={targetPlace.photos || []}/>
       
       <Text style={styles.subtitle}>Region: {targetPlace.region || "-"}</Text>
       <Text><Text style={styles.subtitle}>Description:</Text> {targetPlace.description || "-"}</Text>
       
       <ActivitiesList activities={targetPlace.activities || []}/>

       <ReviewsManager placeID={targetPlace.id || 0} reviews={targetPlace.reviews || []} />
       
      </ScrollView>)
  }
}

const PlaceDetailsScreen = connections(container(PlaceDetails));

PlaceDetailsScreen.navigationOptions = {
  title: "Details"
};

export default PlaceDetailsScreen;

const styles = StyleSheet.create({
  title: {
    fontWeight: 'bold',
    textAlign: 'center',
    fontSize: 20,
    padding: 8,
    marginLeft: 16,
    marginRight: 16,
  },
  subtitle: {
    fontWeight: 'bold',
    textAlign: 'center',
    fontSize: 14,
    padding: 8,
    marginLeft: 16,
    marginRight: 16,},
  image: {
    borderRadius: 1,
    padding: 8,
    height:300, width: 300,

  },
  button: {
    marginLeft: 8,
    marginRight: 8,
  },
})