import React, { Component } from 'react';
import { Button } from 'react-bootstrap';
import PlacePhotosShowcase from '../PlaceDetailsComponents/PlacePhotosShowcase';
import ActivitiesList from '../PlaceDetailsComponents/ActivitiesList';
import ReviewsManager from '../PlaceDetailsComponents/ReviewsManager';
import styled from 'styled-components';
import {container, connections }from '../../containers/PlaceDetails'

class PlaceDetails extends Component {

	render() {	
    let targetPlace = this.props.getTargetPlace(this.props.match.params.placeID);
    let addButton = "";
    if (this.props.auth.uid) {
    addButton = <Button bsStyle="success" onClick={() => this.props.onAddToTripClick(this.props.auth.uid, targetPlace.id)}>Add</Button>
    }
		return (<CenterContainerWithPadding>
      
     	 <h1> {targetPlace.name} {addButton}</h1>

       <PlacePhotosShowcase altForPhotos={targetPlace.name}  photos={targetPlace.photos || []}/>
       
     	 <h3>Region: <small className="text-muted"><p>{targetPlace.region || "-"}</p></small></h3>
     	 <h3>Description: <small className="text-muted"><p>{targetPlace.description || "-"}</p></small></h3>
     	 
     	 <ActivitiesList activities={targetPlace.activities || []}/>

       <ReviewsManager placeID={targetPlace.id || 0} reviews={targetPlace.reviews || []} />
       
      </CenterContainerWithPadding>)
  }
}

export default connections(container(PlaceDetails));

const CenterContainerWithPadding = styled.div`
  margin: 0 auto;
  width: 80%;
`;