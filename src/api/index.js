import {firebaseDb} from './firebase';

const path_places = "/places/";
const path_users = "/users/";
var places_list = [];
var placesFilteredForTrip = [];
var initializedPlaces = false;

const filterPlaces = (places, filter) => {

  switch (filter) {
      case 'all':
        return places;
      default:
        return places.filter(p => p.region === filter); }};


export const fetchPlaces = (filter) =>
	new Promise(function(resolve, reject) {
    if(!initializedPlaces) {
      firebaseDb.ref(path_places).once("value").then((places) => {

      places_list = places.val();
      initializedPlaces = true;

      resolve(filterPlaces(places_list, filter));

    })}

    if (initializedPlaces) {
      resolve(filterPlaces(places_list, filter));
    }
});

  export const forceFetchPlaces = (filter) =>
  new Promise(function(resolve, reject) {
      firebaseDb.ref(path_places).once("value").then((places) => {
      
      places_list = places.val();
      initializedPlaces = true;

      resolve(filterPlaces(places_list, filter));

    })});

export const addReview = (username, userID, placeID, text, positiveVote, image) => 
  new Promise(function(resolve, reject) {

      const review = {
        id: userID,
        content: text,
        positiveVote,
        username,
        image
      };

      console.log(review);
      console.log(path_places + placeID + "/reviews/" + userID);

      firebaseDb.ref(path_places + placeID + "/reviews/" + userID).update(review);
      resolve();
  });

export const addToTrip = (userID, placeID) =>
  new Promise(function(resolve, reject) {

    firebaseDb.ref(path_users + userID + "/trip/" + placeID).update(
     { placeID
    });
    resolve();
  });

export const deleteFromTrip = (userID, placeID) =>
  new Promise(function(resolve, reject) {

    firebaseDb.ref(path_users + userID + "/trip/" + placeID).remove();
    resolve();
  });

export const reOrderTripPlaces = (userID, orderPositionToBeChanged, newOrderPosition) =>
  new Promise(function(resolve, reject) {

    for (var i=0; i<placesFilteredForTrip.length; i++) {
        var placeID = placesFilteredForTrip[i]['id'];
        if (orderPositionToBeChanged === placesFilteredForTrip[i]['order']) {
          placesFilteredForTrip[i]['order'] = newOrderPosition;
          
        } else if (newOrderPosition === placesFilteredForTrip[i]['order']) {
          placesFilteredForTrip[i]['order'] = orderPositionToBeChanged;
        }
        firebaseDb.ref(path_users + userID + "/trip/" + placeID).update(
         { order : placesFilteredForTrip[i]['order']
        });
    }
    
    resolve();
  });

function orderByOrder(a, b) {
        let orderA = a.order;
        let orderB = b.order;
        if (orderA < orderB) {
          return -1;
        }
        if (orderA > orderB) {
          return 1;
        }
        return 0;
      }

const filterPlacesFromTrip = (resolve, places, userID) => {
    let placesWithoutOrder = [];
    let filteredPlaces = false;
    placesFilteredForTrip = [];
    firebaseDb.ref(path_users + userID + "/trip/").once("value").then((tripPlaces) => {

      let placesFromTripInApi = tripPlaces.val();

      for(let placeID in placesFromTripInApi) {
        if(placesFromTripInApi.hasOwnProperty(placeID) && places.hasOwnProperty(placeID)) {
            let placeFullInfo = places[placeID]
            placeFullInfo['order'] = placesFromTripInApi[placeID]['order'];
            if (placeFullInfo['order'] === undefined) {
              placesWithoutOrder.push(placeFullInfo);
            } else {
              placesFilteredForTrip.push(placeFullInfo);
            }
        }
      }

      placesFilteredForTrip.sort(orderByOrder);
      let nextOrder = 1;
      for (let placeWithOrderID in placesFilteredForTrip) {
        placesFilteredForTrip[placeWithOrderID]['order'] = nextOrder++;
      }
      for (let placeWithoutOrderID in placesWithoutOrder) {
        placesWithoutOrder[placeWithoutOrderID]['order'] =  nextOrder++;
        placesFilteredForTrip.push(placesWithoutOrder[placeWithoutOrderID]);
      }

      filteredPlaces = true;
      resolve(placesFilteredForTrip);

    })
    if (filteredPlaces) {
       resolve(placesFilteredForTrip);
    }
   
};

export const fetchPlacesFromUserTrip = (userID) =>
  new Promise(function(resolve, reject) {
    if(!initializedPlaces) {
      firebaseDb.ref(path_places).once("value").then((places) => {

      places_list = places.val();
      initializedPlaces = true;

      filterPlacesFromTrip(resolve, places_list, userID);

    })}

    if (initializedPlaces) {
      filterPlacesFromTrip(resolve, places_list, userID);
    }
});



  