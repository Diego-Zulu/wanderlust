import * as firebase from 'firebase'

var config = {
    apiKey: "AIzaSyDRN_-_aaWu9sUvUR8CbCyp5MUpFha9P4k",
    authDomain: "wanderlust-backend.firebaseapp.com",
    databaseURL: "https://wanderlust-backend.firebaseio.com",
    projectId: "wanderlust-backend",
    storageBucket: "wanderlust-backend.appspot.com",
    messagingSenderId: "758087651764"
  };
  
firebase.initializeApp(config);

export const firebaseDb = firebase.database();

export const firebaseAuth = firebase.auth();

export const firebaseStorage = firebase.storage();